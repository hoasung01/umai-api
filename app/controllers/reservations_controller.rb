class ReservationsController < ApplicationController
  before_action :set_restaurant

  def index
    reservations = Reservation.all
    json_response(reservations)
  end

  def create
    reservation = Reservation.new(reservation_params)

    if reservation.save
      GuestMailer.make_a_reservation(reservation).deliver_later
      RestaurantMailer.has_a_reservation(reservation).deliver_later
      json_response(reservation, :created)
    else
      json_response({ message: reservation.errors }, :unprocessable_entity)
    end
  end

  def update
    reservation = Reservation.find(params[:id])

    if reservation.update_attributes(reservation_params)
      GuestMailer.update_a_reservation(reservation).deliver_later
      json_response({ head: :no_content }, :ok)
    else
      json_response({ message: reservation.errors }, :unprocessable_entity)
    end
  end

  private

  def reservation_params
    params.require(:reservation)
          .permit(:reservation_time, :guest_count, :guest_id, :table_id, :shift_id)
  end

  def set_restaurant
    @restaurant ||= Restaurant.find(params[:restaurant_id])
  end
end