class Reservation < ApplicationRecord
  belongs_to :guest
  belongs_to :table
  belongs_to :shift
  validate :reservation_time_must_lie_within_restaurant_shift
  validate :guest_count_must_lie_within_restaurant_table_capacity

  private

  def reservation_time_must_lie_within_restaurant_shift
    errors.add :reservation_time unless reservation_time.between?(shift.from_seconds_in_a_day, shift.to_seconds_in_a_day)
  end

  def guest_count_must_lie_within_restaurant_table_capacity
    errors.add :guest_count unless guest_count.between?(table.min_seat, table.max_seat)
  end
end
