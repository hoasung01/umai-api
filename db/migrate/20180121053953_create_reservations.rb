class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.integer :reservation_time
      t.integer :guest_count, default: 0
      t.references :guest, foreign_key: true
      t.references :table, foreign_key: true
      t.references :shift, foreign_key: true

      t.timestamps
    end
  end
end
