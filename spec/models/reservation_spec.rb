require 'rails_helper'

describe Reservation do
  context 'associations' do
    it { should belong_to(:guest) }
    it { should belong_to(:table) }
    it { should belong_to(:shift) }
  end

  context 'validations' do
    it 'should be valid if reservation time lie within restaurant shift' do
      shift = create(:shift, from_seconds_in_a_day: 34_200, to_seconds_in_a_day: 46_800)
      reservation = build(:reservation, reservation_time: 40_000, shift: shift)
      expect(reservation).to be_valid
    end

    it 'should not be valid if reservation time not lie within restaurant shift' do
      shift = create(:shift, from_seconds_in_a_day: 34_200, to_seconds_in_a_day: 46_800)
      reservation = build(:reservation, reservation_time: 30_000, shift: shift)
      expect(reservation).not_to be_valid
    end

    it 'should be valid if guest count lie within restaurant table capacity' do
      table = create(:table, min_seat: 2, max_seat: 5)
      reservation = build(:reservation, guest_count: 2, table: table)
      expect(reservation).to be_valid
    end

    it 'should be valid if guest count not lie within restaurant table capacity' do
      table = create(:table, min_seat: 2, max_seat: 5)
      reservation = build(:reservation, guest_count: 6, table: table)
      expect(reservation).not_to be_valid
    end
  end
end
