class ApplicationPolicy
  attr_reader :guest, :record

  def initialize(guest, record)
    raise Pundit::NotAuthorizedError, I18n.t('api.errors.message.guest_must_login') unless guest

    @guest = guest
    @record = record
  end

  def index?
    false
  end

  def show?
    scope.where(id: record.id).exists?
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    false
  end

  def edit?
    update?
  end

  def destroy?
    false
  end

  def scope
    Pundit.policy_scope!(guest, record.class)
  end

  class Scope
    attr_reader :guest, :scope

    def initialize(guest, scope)
      @guest = guest
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
