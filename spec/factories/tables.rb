FactoryBot.define do
  factory :table do
    name FFaker::Company.name
    min_seat Random.rand(2)
    max_seat Random.rand(2)
    restaurant
  end
end
