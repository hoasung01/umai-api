class GuestMailer < ApplicationMailer
  def make_a_reservation(reservation)
    @guest = guest_for(reservation)
    mail(to: @guest.email, subject: 'You make a reservation')
  end

  def update_a_reservation(reservation)
    @guest = guest_for(reservation)
    @reservation_tracking_info = 
        ReservationTrackingInfo.new(reservation.reservation_time_was,
                                    reservation.reservation_time,
                                    reservation.guest_count_was,
                                    eservation.guest_count)
    mail(to: @guest.email, subject: 'You have just updated a reservation')
  end

  private

  def guest_for(reservation)
    Guest.find(reservation.guest_id)
  end

  ReservationTrackingInfo = Struct.new(:reservation_time_old, :reservation_time_new,
                                       :guest_count_old, :guest_count_new)
end
