FactoryBot.define do
  factory :restaurant do
    name FFaker::Company.name
    email FFaker::Internet.email
    phone "601#{Random.rand.to_s[2...10]}"
  end
end
