FactoryBot.define do
  factory :shift do
    from_seconds_in_a_day 34_200
    to_seconds_in_a_day 46_800
    shift_type Random.rand(2)
    restaurant
  end
end
