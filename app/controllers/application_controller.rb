class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :guest_not_authorized

  private

  def guest_not_authorized
    json_response({ message: I18n.t('api.errors.message.guest_not_authorized') }, :forbidden)
  end
end
