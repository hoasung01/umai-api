class Shift < ApplicationRecord
  belongs_to :restaurant
  enum shift_type: { morning: 0, evening: 1 }
  validate :start_time_must_not_greater_than_end_time

  private

  def start_time_must_not_greater_than_end_time
    errors.add :from_seconds_in_a_day if from_seconds_in_a_day >= to_seconds_in_a_day
  end
end
