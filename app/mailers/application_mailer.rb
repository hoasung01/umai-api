class ApplicationMailer < ActionMailer::Base
  default from: 'umai@example.com'
  layout 'mailer'
end
