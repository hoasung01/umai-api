require 'rails_helper'

describe Shift do
  it { should define_enum_for(:shift_type) }
  
  context 'associations' do
    it { should belong_to(:restaurant) }
  end

  context 'validations' do
    it 'should be valid if start time less than end time' do
      shift = build(:shift, from_seconds_in_a_day: 34200, to_seconds_in_a_day: 46800)
      expect(shift).to be_valid
    end

    it 'should not be valid if start time greater or equal than end time' do
      shift = build(:shift, from_seconds_in_a_day: 46800, to_seconds_in_a_day: 34200)
      expect(shift).not_to be_valid
    end
  end
end
