require 'rails_helper'

describe Guest do
  context 'associations' do
    it { should have_many(:reservations).dependent(:destroy) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
  end
end
