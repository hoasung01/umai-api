class Guest < ApplicationRecord
  validates :name, :email, presence: true
  validates :email, uniqueness: true
  has_many :reservations, dependent: :destroy
end
