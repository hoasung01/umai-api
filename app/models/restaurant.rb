class Restaurant < ApplicationRecord
  validates :name, :email, :phone, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validates :phone, format: { with: /\A(6?01\d{8})\z/ }
  has_many :shifts, dependent: :destroy
  has_many :tables, dependent: :destroy
end