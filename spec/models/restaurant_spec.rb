require 'rails_helper'

describe Restaurant do
  context 'associations' do
    it { should have_many(:shifts).dependent(:destroy) }
    it { should have_many(:tables).dependent(:destroy) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:phone) }
    it { should allow_value('valid-email@example.com').for(:email) }
    it { should_not allow_value('not-valid-email.exampe.com').for(:email) }
    it { should allow_value("601#{Random.rand.to_s[2...10]}").for(:phone) }
    it { should_not allow_value("601#{Random.rand.to_s[2..10]}").for(:phone) }
  end
end
