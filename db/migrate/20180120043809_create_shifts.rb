class CreateShifts < ActiveRecord::Migration[5.1]
  def change
    create_table :shifts do |t|
      t.integer :from_seconds_in_a_day
      t.integer :to_seconds_in_a_day
      t.integer :shift_type
      t.references :restaurant, foreign_key: true

      t.timestamps
    end
  end
end
