class CreateTables < ActiveRecord::Migration[5.1]
  def change
    create_table :tables do |t|
      t.string :name, default: ''
      t.integer :min_seat, default: 0
      t.integer :max_seat, default: 0
      t.references :restaurant, foreign_key: true

      t.timestamps
    end
  end
end
