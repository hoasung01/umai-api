Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :restaurants, only: [] do
    resources :reservations, only: [:create, :update, :index]
  end
end
