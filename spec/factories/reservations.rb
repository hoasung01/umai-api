FactoryBot.define do
  factory :reservation do
    reservation_time 34200
    guest_count 2
    association :guest, email: FFaker::Internet.email
    association :table, min_seat: 1, max_seat: 5
    shift
  end
end
