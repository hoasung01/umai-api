FactoryBot.define do
  factory :guest do
    name FFaker::Internet.user_name
    email FFaker::Internet.email
  end
end
