class RestaurantMailer < ApplicationMailer
  def has_a_reservation(reservation)
    @restaurant = restaurant_for(reservation)
    mail(to: @restaurant.email, subject: 'You has a reservation')
  end

  private

  def table_from(reservation)
    Table.find(reservation.table_id)
  end

  def restaurant_for(reservation)
    Restaurant.find(table_from(reservation).restaurant_id)
  end
end
